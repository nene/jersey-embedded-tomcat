# Jersey with Embedded Tomcat

Sample project for a JAX-RS/Jersey with Embedded Tomcat.

The purpose of this is so that you do not need to download and run Tomcat
seperately and then have to deploy the war file to Tomcat.

This is an app in a single bundle.

## Technologies used

- [Maven 3.6.0](https://maven.apache.org/)
- [Maven Repository](https://mvnrepository.com/)
- [Tomcat 9 Embedded](https://tomcat.apache.org/download-90.cgi)
- [Jersey implementation of JAR-RS v2.25.1](https://jersey.github.io/)

## Quick App Build

#### maven command to generate project structure
```
mvn archetype:generate \
-DarchetypeGroupId=org.glassfish.jersey.archetypes \
-DarchetypeArtifact=jersey-quickstart-webapp \
-DarchetypeVersion=2.25.1 
```

> NOTE: See the pom.xml file for details.

## Running the App

By default, the application will run at port `8090`

If you want to change the port, set environment variable `PORT`
```
export PORT=8585
```

After the application is executed, go to:
```
http://localhost:8090/

or

http://localhost:8090/webapi/
```

## Reference Links

- [Reference Webapp](http://www.sortedset.com/embedded-tomcat-jersey)


