package com.acme.web.jaxrs;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Optional;
import javax.servlet.ServletException;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class WebApp {

  private static final Optional<String> webServerPort = Optional.ofNullable(System.getenv("PORT"));

  public static void main(String[] args) throws Exception, LifecycleException {
    new WebApp().runWebServer();
  }

  public void runWebServer() throws ServletException, LifecycleException, MalformedURLException {
    String contextPath = "/";

    // Create Tomcat Server
    Tomcat tomcatServer = new Tomcat();

    // Bind the port to Tomcat Server
    tomcatServer.setPort(Integer.valueOf(webServerPort.orElse("8090")));

    // Define a web application context
    Context ctx = tomcatServer.addWebapp(contextPath, new File("src/main/webapp").getAbsolutePath());

    // Define and bind web.xml file location
    File configFile = new File(ctx.getDocBase() + "/WEB-INF/web.xml");
    ctx.setConfigFile(configFile.toURI().toURL());

    // Start Tomcat process and wait for requests
    tomcatServer.start();
    tomcatServer.getServer().await();

  }

}
